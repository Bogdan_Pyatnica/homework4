package ua.piatnytsia.homework.four.two.processor;


import org.springframework.batch.item.ItemProcessor;
import ua.piatnytsia.homework.four.two.models.User;

public class UserItemProcessor implements ItemProcessor<User, User> {
    @Override
    public User process(User user) throws Exception {

        if (user.getBalance()<10){
            System.out.println("Sending email to User: " + user.getEmail() + " , "+ user.getName());
        }

        return user;
    }
}