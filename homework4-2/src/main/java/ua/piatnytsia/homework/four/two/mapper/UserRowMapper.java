package ua.piatnytsia.homework.four.two.mapper;

import org.springframework.jdbc.core.RowMapper;
import ua.piatnytsia.homework.four.two.models.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = User.getUser();
        user.setId(resultSet.getInt("id"));
        user.setName(resultSet.getString("name"));
        user.setEmail(resultSet.getString("email"));
        user.setBalance(resultSet.getInt("balance"));
        return user;
    }
}