package ua.piatnytsia.homework.four.two.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@ComponentScan(value = {"ua.piatnytsia.homework.four.two"})
public class Homework4Application {

    public static void main(String[] args) {
        SpringApplication.run(Homework4Application.class, args);
    }

}
