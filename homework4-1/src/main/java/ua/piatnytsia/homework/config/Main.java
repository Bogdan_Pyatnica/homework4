package ua.piatnytsia.homework.config;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.integration.channel.DirectChannel;
import ua.piatnytsia.homework.models.Order;
import ua.piatnytsia.homework.services.OrderServiceImpl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Order> ordersFromCSV = new ArrayList<>();
        final AbstractApplicationContext context = new AnnotationConfigApplicationContext(MyIntegrationConfig.class);
        context.registerShutdownHook();
        DirectChannel outputChannel = context.getBean("outputChannel", DirectChannel.class);
        outputChannel.subscribe(message ->
                System.out.println(new OrderServiceImpl().saveToList((List<Order>) message.getPayload(), ordersFromCSV))
        );
        context.getBean(MyIntegrationConfig.GetOrder.class).getOrders(new File("orders.csv"));

        System.exit(0);
    }


}
