package ua.piatnytsia.homework.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import ua.piatnytsia.homework.models.Order;

import java.io.File;
import java.util.Collection;

@Configuration
@EnableIntegration
@IntegrationComponentScan
@ComponentScan(basePackages = {"ua.piatnytsia.homework"})

public class MyIntegrationConfig {

    @Bean(name = "outputChannel")
    DirectChannel outputChannel() {
        return new DirectChannel();
    }

    @MessagingGateway
    public interface GetOrder {
        @Gateway(requestChannel = "orderFlow.input")
        Collection<Order> getOrders(File file);
    }

    @Bean
    public IntegrationFlow orderFlow() {
        return IntegrationFlows.from("orderFlow.input")
                .handle("orderServiceImpl", "getOrders")
                .split()
                .log()
                .filter((Order o) -> !o.isCanceled())
                .log()
                .aggregate(a -> a.releaseStrategy(group -> group.size() >= 10)
                        .expireGroupsUponCompletion(true)
                        .groupTimeout(500)
                        .sendPartialResultOnExpiry(true))
                .channel("outputChannel")
                .get();
    }

}
