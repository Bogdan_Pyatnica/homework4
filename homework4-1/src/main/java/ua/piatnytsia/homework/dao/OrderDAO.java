package ua.piatnytsia.homework.dao;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import org.springframework.stereotype.Component;
import ua.piatnytsia.homework.models.Order;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class OrderDAO {
    public List<Order> getOrdersFromCSV(File file) {
        List<Order> orders = new ArrayList<>();
        try (CSVReader reader = new CSVReader(new FileReader(file))) {
            String[] values;
            while ((values = reader.readNext()) != null) {
                for (String s : values) {
                    Order order = new Order();
                    order.setOrderState(s);
                    orders.add(order);
                }
            }
        } catch (IOException | CsvValidationException e) {
            e.printStackTrace();
        }
        return orders;
    }


}
