package ua.piatnytsia.homework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.piatnytsia.homework.dao.OrderDAO;
import ua.piatnytsia.homework.models.Order;

import java.io.File;
import java.util.List;

@Service("orderServiceImpl")
public class OrderServiceImpl implements OrderService {
    OrderDAO orderDAO = new OrderDAO();
    @Autowired
    private OrderServiceImpl(OrderDAO orderDAO){
        this.orderDAO=orderDAO;
    }

    public OrderServiceImpl() {
    }



    @Override
    public List<Order> getOrders(File file) {
        return orderDAO.getOrdersFromCSV(file);
    }

    @Override
    public String transform(Order order) {
        return order.getOrderState().toString();
    }

    @Override
    public List<Order> saveToList(List<Order> order, List<Order> list) {
        list.addAll(order);
        return list;
    }
}
