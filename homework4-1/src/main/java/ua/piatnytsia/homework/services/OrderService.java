package ua.piatnytsia.homework.services;

import ua.piatnytsia.homework.models.Order;

import java.io.File;
import java.util.List;

public interface OrderService {
    List<Order> getOrders(File filename);

    String transform(Order order);


    List<Order> saveToList(List<Order> message, List<Order> list);
}
