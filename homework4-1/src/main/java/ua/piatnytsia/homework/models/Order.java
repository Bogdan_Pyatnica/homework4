package ua.piatnytsia.homework.models;

import java.util.Objects;

public class Order {
    private OrderState orderState;

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public void setOrderState(String orderState){
        this.orderState=OrderState.valueOf(orderState);
    }

    public OrderState getOrderState() {
        return orderState;
    }


    public boolean isCanceled() {
        return this.getOrderState() == OrderState.CANCELED;
    }

    @Override
    public String toString() {
        return "Order with orderState:" + orderState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderState == order.orderState;
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderState);
    }
}
