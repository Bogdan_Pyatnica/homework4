package ua.piatnytsia.homework.tests;

import org.junit.Before;
import org.junit.Test;
import ua.piatnytsia.homework.models.Order;
import ua.piatnytsia.homework.models.OrderState;
import ua.piatnytsia.homework.services.OrderServiceImpl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OrderServiceImplTest {
    File testFile = new File("test.csv");
    List<Order> ordersList = new ArrayList<>();
    OrderServiceImpl orderService = new OrderServiceImpl();

    @Before
    public void setUp() {
        Order order1 = new Order();
        Order order2 = new Order();
        Order order3 = new Order();
        order1.setOrderState(OrderState.CANCELED);
        order2.setOrderState(OrderState.WAITING_FOR_PAYMENT);
        order3.setOrderState(OrderState.PAYMENT_COMPLETED);
        Collections.addAll(ordersList, order1, order2, order3);
    }

    @Test
    public void shouldReturnListOfOrders() {
        List<Order> testList = orderService.getOrders(testFile);
        assertTrue(ordersList.containsAll(testList));
    }

    @Test
    public void shouldTransformIntoString() {
        Order order = new Order();
        String testString = "PAYMENT_COMPLETED";
        order.setOrderState(OrderState.PAYMENT_COMPLETED);
        assertEquals(testString, orderService.transform(order));
    }


    @Test
    public void shouldSaveToList() {
        List<Order> emptyOrders = new ArrayList<>();
        orderService.saveToList(ordersList, emptyOrders);
        assertEquals(ordersList, emptyOrders);
    }
}